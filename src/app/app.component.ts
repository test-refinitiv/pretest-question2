import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'question2';
  filter: string = "";
  list: string[] = [];

  ngOnInit() {
    this.getList();
  }

  async getList() {
    const url = "https://api.publicapis.org/categories";
    fetch(url).then((response) => {
      return response.json();
    }).then((data) => {
      this.list = data.categories;
    });
  }

  async handleChangeText(event: any) {
    const value = event.target.value.toLocaleLowerCase();
    this.filter = value;
  }
}
